import java.lang.*;

public class SubKeys
{
	private final byte[] PC_ONE_ARRAY =
	{
		57,	49,	41,	33,	25,	17,	9,	1,	58,	50,	42,	34,	26,	18,
		10,	2,	59,	51,	43,	35,	27,	19,	11,	3,	60,	52,	44,	36,
		63,	55,	47,	39,	31,	23,	15,	7,	62,	54,	46,	38,	30,	22,
		14,	6,	61,	53,	45,	37,	29,	21,	13,	5,	28,	20,	12,	4
	};

	private final byte[] PC_TWO_ARRAY =
	{
		14,	17,	11,	24,	1,	5,	3,	28,	15,	6,	21,	10,
		23,	19,	12,	4,	26,	8,	16,	7,	27,	20,	13,	2,
		41,	52,	31,	37,	47,	55,	30,	40,	51,	45,	33,	48,
		44,	49,	39,	56,	34,	53,	46,	42,	50,	36,	29,	32
	};

	private final byte[] ROTATIONS_ARRAY = 
	{
		1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1
	};

	private long key;
	private long subkeys[];

	public SubKeys()
	{
		key = 0;
		subkeys = new long[16];
	}

	public SubKeys(long inKey)
	{
		key = inKey;
		subkeys = new long[16];
	}

	public long[] getSubKeys()
	{
		int cHalf, dHalf;
		long refinedKey;

		//subkey permutated choice 1 returns 56 bit key
		refinedKey = subKeyPC1(key);

		// split into 28-bit left and right (c and d) pairs.
		cHalf = (int) (refinedKey>>>28);
		dHalf = (int) (refinedKey&0x0FFFFFFF);


		for (int i=0; i<16; i++) 
		{
			// check to see if this bit should be shifted one bit to left
			if (ROTATIONS_ARRAY[i] == 1) 
			{
				// rotate by 1 bit
				cHalf = ((cHalf<<1) & 0x0FFFFFFF) | (cHalf>>27);
				dHalf = ((dHalf<<1) & 0x0FFFFFFF) | (dHalf>>27);
			} 
			//else this bit must be shifted two bits to left
			else 
			{
				// rotate by 2 bits
				cHalf = ((cHalf<<2) & 0x0FFFFFFF) | (cHalf>>26);
				dHalf = ((dHalf<<2) & 0x0FFFFFFF) | (dHalf>>26);
			}
			
			// join the two keystuff halves together.
			long cd = (cHalf&0xFFFFFFFFL)<<28 | (dHalf&0xFFFFFFFFL);
		

			// perform the PC2 permutation
			subkeys[i] = subKeyPC2(cd);
			
			Conversions convert = new Conversions();
			

			String iterText = Long.toBinaryString(subkeys[i]);

			while (iterText.length() < 48)
			{
			    iterText = "0" + iterText;
			}

		}
		
		return subkeys;
	}

	private long subKeyPC1(long inKey) 
	{
		int keyPosition;
		long positionedKey, shiftedRefinedKey, refinedKey = 0;

		//for each bit in PC one table (56 as ignoring parity bits)
		for (int ii = 0; ii < PC_ONE_ARRAY.length; ii++) 
		{
			//get key position from the PC one table
			keyPosition = 64 - PC_ONE_ARRAY[ii];

			//get shifted key one bit to left of refined key
			shiftedRefinedKey = refinedKey << 1;

			//shift the key to the position indicated from PC1 table
			positionedKey = (inKey >> keyPosition & 0x01);

			//refined key is the or of shiftedRefinedKey and positionedKey
			refinedKey = shiftedRefinedKey | positionedKey;
		}

		//return final refined key
		return refinedKey;
	}

	private long subKeyPC2(long inKey) 
	{
		int keyPosition;
		long shiftKey, shiftedRefinedKey, refinedKey = 0;

		//for each bit in PC two table (48 bits)
		for (int ii = 0; ii < PC_TWO_ARRAY.length; ii++) 
		{
			//get key position from PC two table
			keyPosition = 56 - PC_TWO_ARRAY[ii];

			//get shifted key one bit to left of refined key
			shiftedRefinedKey = (refinedKey << 1);

			//shift the key to the position indicated in PC2 table
			shiftKey = (inKey >> keyPosition & 0x01);

			//refined key is the or of shifted key and key 
			//in position given by PC2 table
			refinedKey = shiftedRefinedKey | shiftKey;
		}

		//return refined key
		return refinedKey;
	}

}