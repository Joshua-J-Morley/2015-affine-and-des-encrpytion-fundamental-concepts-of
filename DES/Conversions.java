import java.lang.*;
import java.util.*;
import java.nio.*;
import java.io.UnsupportedEncodingException;


public class Conversions
{
	public Conversions()
	{
	}

	public long getLongFromStr(String inString)
	{
		long outLong = 0;
		try
		{
			outLong = Long.parseLong(inString);
		}
		catch(NumberFormatException e)
		{
			System.out.println(""+e);	
		}
		return outLong;
	}


	public long getLongFromBytes(byte[] inByteArray, int inStart) 
	{
		long retLong = 0;

		for (int ii=0; ii<8; ii++) 
		{
			byte value;

			if ((inStart+ii) < inByteArray.length) 
			{
				value = inByteArray[inStart+ii];
			} 
			else 
			{
				value = 0;
			}

			retLong = retLong <<8 | (value & 0xFFL);
		}
		
		return retLong;

	}

	public byte[] getBytesFromLong(long inLong) 
	{
		byte[] retByteArray = ByteBuffer.allocate(8).putLong(inLong).array();

		String byteArray = new String(retByteArray);

		return retByteArray;
	}

	public String combineByteArray(String inArrOne, byte[] inArrTwo)
	{
		String retString = new String("");
		String arrayTwo = new String(inArrTwo);

		retString = inArrOne + arrayTwo;

		return retString;	
	}

	public byte[] accumulateByteArray(byte[] inLargeArr, byte[] inNewArr)
	{
		int sizeOfNewArray, start;
		byte[] outFinalArr;

		if(inLargeArr == null)
		{
			sizeOfNewArray = inNewArr.length;
			start = 0;
		}
		else
		{
			sizeOfNewArray = inLargeArr.length + inNewArr.length;
			start = inLargeArr.length;
		}

		outFinalArr = new byte[sizeOfNewArray];
		
		if(inLargeArr != null)
		{
			for (int ii = 0; ii < inLargeArr.length; ii ++) 
			{
				outFinalArr[ii] = inLargeArr[ii];
			}
		}
		for (int ii = 0; ii < inNewArr.length; ii ++) 
		{
			outFinalArr[start + ii] = inNewArr[ii];	
		}

		return outFinalArr;
	}

	public void printByteArrayAsBinary(byte[] inByteArray)
	{
		for (byte b : inByteArray) 
		{
			System.out.print(Integer.toBinaryString(b & 255 | 256).substring(1));
		}
		System.out.println("");
	}

	public byte[] shrink(byte[] inByteArray, int inIndex)
	{
		byte[] retByteArray = new byte[inIndex];

		for (int ii = 0; ii < inIndex; ii++) 
		{
			retByteArray[ii] = inByteArray[ii];	
		}

		return retByteArray;
	}

}