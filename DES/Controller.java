import java.lang.*;
import java.lang.Byte.*;
import java.math.*;
import java.io.*;
import java.util.*;
import java.nio.*;

public class Controller
{
	
//need to pad
	public static void main(String[] args) 
	{
		Encrypt encrypt = new Encrypt();
		Decrypt decrypt = new Decrypt();
		
		//test();

		test2();
	}

	public static void test()
	{
		Encrypt encrypt = new Encrypt();
		Decrypt decrypt = new Decrypt();
		byte[] plainText = null;
		byte[] readCipherText = null;
		byte[] key = "thirteen".getBytes();
		String output = "";

		try
		{
			plainText = readFile("nblah.txt");
		}
		catch(IOException e)
		{
			System.out.println("Could not read from file" + e.getMessage());
		}

		byte[] cipherText = encrypt.encryptMessage(plainText, key);

		//readCipherText = cipherText;

		byte[] retPlainText = decrypt.decryptMessage(readCipherText, key);

		//System.out.println("Final Plain Text\n"+new String(retPlainText));
		
		try
		{
			writeFile("output.txt", retPlainText);
		}
		catch(IOException e)
		{
			System.out.println("Could not write to file" + e.getMessage());
		}
	}

	public static void test2()
	{
		Encrypt encrypt = new Encrypt();
		Decrypt decrypt = new Decrypt();
		byte[] plainText = null;
		byte[] readCipherText = null;
		byte[] key = "11111111".getBytes();
		String output = "";

		try
		{

			plainText = readFile("testfile.txt");
		}
		catch(IOException e)
		{
			System.out.println("Could not read from file" + e.getMessage());
		}
		byte[] cipherText = encrypt.encryptMessage(plainText, key);

	
		try
		{
			writeFile("cipherText.txt", cipherText);
		}
		catch(IOException e)
		{
			System.out.println("Could not write to file" + e.getMessage());
		}


		try
		{
			readCipherText = readCipherFile("cipherText.txt");
		}
		catch(IOException e)
		{
			System.out.println("Could not read from file" + e.getMessage());
		}

		byte[] retPlainText = decrypt.decryptMessage(readCipherText, key);

		try
		{
			writeFile("output.txt", retPlainText);
		}
		catch(IOException e)
		{
			System.out.println("Could not write to file" + e.getMessage());
		}
		
	}


	public static byte[] readFile(String inFileName) throws IOException
	{
		Conversions convert = new Conversions();
		byte[] outPlainText = null;
		File file = new File(inFileName);
		FileInputStream reader = new FileInputStream(file);
		int lengthOfFile;

		if((int)(file.length()) % 64 == 0)
		{
			lengthOfFile = (int)(file.length())+64;
		}
		else
		{
			lengthOfFile = 64-(int)(file.length())%64;
			lengthOfFile = lengthOfFile + (int)(file.length());
		}

		outPlainText = new byte[lengthOfFile];

		reader = new FileInputStream (inFileName);
		reader.read(outPlainText,0,outPlainText.length);
		reader.close();

		outPlainText[(int)file.length()-1] = 127;

		return outPlainText;
	}

	public static byte[] readCipherFile(String inFileName) throws IOException
	{
		Conversions convert = new Conversions();
		byte[] outCipherText = null;
		File file = new File(inFileName);
		FileInputStream reader = new FileInputStream(file);

		outCipherText = new byte[(int)file.length()];

		reader = new FileInputStream (inFileName);
		reader.read(outCipherText,0,outCipherText.length);
		reader.close();

		return outCipherText;
	}

	public static void writeFile(String inFileName, byte[] inText) throws IOException
	{
		FileOutputStream writer = new FileOutputStream(inFileName);
		writer.write(inText);
		writer.close();
	}
}