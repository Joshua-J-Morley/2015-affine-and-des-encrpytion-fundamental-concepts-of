import java.lang.*;
import java.lang.Long.*;
import java.util.*;
import java.nio.*;

public class Encrypt
{
	Permutations perm;
	Conversions convert;

	public Encrypt()
	{
		perm = new Permutations();
		convert = new Conversions();
	}

	public byte[] encryptMessage(byte[] inMessage, byte[] inKey)
	{
		int mm=0;
		long msgLong, keyLong, cipherLong = 0L;
		byte[] cText;
		byte[] message;
		byte[] key;
		String cipherText = new String("");
		byte[] iterText = null;
		byte[] retCText = null;
		ByteBuffer byteBuff = ByteBuffer.allocate(inMessage.length);

		// encrpt each 8 bytes of message
		for (int ii=0; ii<inMessage.length/8; ii++) 
		{
			//for each 64 bits (8 bytes) of inMessage
			//get inMessage and key as long (64bit int)
			msgLong = convert.getLongFromBytes(inMessage, ii*8);
			keyLong = convert.getLongFromBytes(inKey, 0);

			cipherLong = encryptBlock(msgLong, keyLong);
			
			iterText = convert.getBytesFromLong(cipherLong);
			retCText = convert.accumulateByteArray(retCText, iterText);
		}

		return retCText;
	}

	private long encryptBlock(long m, long key) 
	{
		int lSide, rSide, prevRside, prevLSide;
		long initialPerm, concatRL, finalPerm;
		long[] subkeys = new long[16];
		Switch switchFunc = new Switch();
		Feistel feistelFunc = new Feistel();
		
		SubKeys sKey = new SubKeys(key);
		subkeys = sKey.getSubKeys();

		initialPerm = perm.initialPermutation(m);

		lSide = switchFunc.getLeftSide(initialPerm);
		rSide = switchFunc.getRightSide(initialPerm);
		
		for (int ii =0; ii <16; ii++) 
		{
			prevLSide = lSide;
			lSide = rSide;

			rSide = prevLSide ^ feistelFunc.feistel(rSide, subkeys[ii]);
		}
		
		concatRL = switchFunc.concatToRL(lSide, rSide);
		
		finalPerm = perm.finalPermutation(concatRL);
		
		return finalPerm;
	}
}