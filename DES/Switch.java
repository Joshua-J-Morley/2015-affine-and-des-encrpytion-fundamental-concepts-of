

public class Switch
{
	public Switch()
	{
	}

	public int getLeftSide(long inMessage)
	{
		int lSide;

		//shift left half of message over and truncate
		//as integer to get 32 bits
		lSide = (int)(inMessage >> 32);

		return lSide;
	}

	public int getRightSide(long inMessage)
	{
		int rSide;

		//truncate bitwise and at 32 bits to get right
		rSide = (int)(inMessage & 0xFFFFFFFFL);

		return rSide;
	}

	public long concatToRL(int inLSide, int inRSide)
	{
		long outConcatRL;

		//shift right side left by 32 bits and OR with left side
		outConcatRL = (inRSide&0xFFFFFFFFL)<<32 | (inLSide&0xFFFFFFFFL);

		return outConcatRL;
	}
}