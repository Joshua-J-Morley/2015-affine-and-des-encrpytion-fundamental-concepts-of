import java.lang.*;

public class Permutations
{
	private static final byte[] IP_ARRAY = 
	{ 
		58,	50,	42,	34,	26,	18,	10,	2,	60,	52,	44,	36,	28,	20,	12,	4,
		62,	54,	46,	38,	30,	22,	14,	6,	64,	56,	48,	40,	32,	24,	16,	8,
		57,	49,	41,	33,	25,	17,	9, 	1,	59,	51,	43,	35,	27,	19,	11,	3,
		61,	53,	45,	37,	29,	21,	13,	5,	63,	55,	47,	39,	31,	23,	15,	7
	};

	private static final byte[] FP_ARRAY = 
	{
		40,	8,	48,	16,	56,	24,	64,	32,	39,	7,	47,	15,	55,	23,	63,	31,
		38,	6,	46,	14,	54,	22,	62,	30,	37,	5,	45,	13,	53,	21,	61,	29,
		36,	4,	44,	12,	52,	20,	60,	28,	35,	3,	43,	11,	51,	19,	59,	27,
		34,	2,	42,	10,	50,	18,	58,	26,	33,	1,	41,	9,	49,	17,	57,	25
	};

	public Permutations()
	{
	}

	public long initialPermutation(long inMessage) 
	{
		int messagePosition;
		long shiftedM, positionedM, permutedM = 0;

		for (int ii = 0; ii < IP_ARRAY.length; ii++) 
		{
			//get bits position in permuted message
			messagePosition = 64 - IP_ARRAY[ii];

			//shitft message 1 bit left
			shiftedM = (permutedM << 1);

			//shift message into position
			positionedM = (inMessage >> messagePosition & 0x01);

			//get permuted positionn from shifted and positioned
			permutedM = shiftedM | positionedM;
		}

		//return permuted message
		return permutedM;
	}

	public long finalPermutation(long inMessage) 
	{
		int messagePosition;
		long shiftedM, positionedM, permutedM = 0;

		for (int ii = 0; ii < FP_ARRAY.length; ii++) 
		{
			//get bits position in permuted message
			messagePosition = 64 - FP_ARRAY[ii];

			//shitft message 1 bit left
			shiftedM = (permutedM << 1);

			//shift message into position
			positionedM = (inMessage >> messagePosition & 0x01);

			//get permuted positionn from shifted and positioned
			permutedM = shiftedM | positionedM;
		}

		//return permuted message
		return permutedM;
	}
}