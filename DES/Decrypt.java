import java.lang.*;
import java.lang.Long.*;
import java.util.*;
import java.nio.*;

public class Decrypt
{
	Permutations perm;
	Conversions convert;

	public Decrypt()
	{
		perm = new Permutations();
		convert = new Conversions();
	}

	public byte[] decryptMessage(byte[] inMessage,byte[] inKey)
	{
		int mm = 0, ii;
		long msgLong, keyLong, plainLong;
		byte[] message;
		byte[] key;
		byte[] iterText = null;
		byte[] retPText = null;
		byte temp=110;
		ByteBuffer byteBuff = ByteBuffer.allocate(inMessage.length);


		// encrpt each 8 bytes of message
		for (ii=0; ii<inMessage.length/8; ii++) 
		{
			//for each 64 bits (8 bytes) of inMessage
			//get inMessage and key as long (64bit int)
			msgLong = convert.getLongFromBytes(inMessage, ii*8);		
			keyLong = convert.getLongFromBytes(inKey, 0);

			plainLong = decryptBlock(msgLong, keyLong);
	
			iterText = convert.getBytesFromLong(plainLong);

			retPText = convert.accumulateByteArray(retPText, iterText);
		}

		for (ii = ((retPText.length)-1); ii > 0 && temp != 127; ii--) 
		{
			temp = retPText[ii];
		}

		retPText = convert.shrink(retPText, ii+2);

		return retPText;
	}

	private long decryptBlock(long m, long key) 
	{
		int lSide , rSide , prevRside,prevLSide ;
		long initialPerm, concatRL, finalPerm;
		long[] subkeys = new long[16];
		Switch switchFunc = new Switch();
		Feistel feistelFunc = new Feistel();
		
		SubKeys sKey = new SubKeys(key);

		// generate the 16 subkeys
		subkeys = sKey.getSubKeys();

		// perform the initial permutation
		initialPerm = perm.initialPermutation(m);
		//System.out.println("\n initial: " + initialPerm);

		lSide = switchFunc.getLeftSide(initialPerm);
		rSide = switchFunc.getRightSide(initialPerm);

		// perform 16 rounds
		for (int ii = 15; ii >=0; ii --) 
		{
			prevLSide = lSide;

			// the right half becomes the new left half.
			lSide = rSide;

			// the Feistel function is applied to the old left half
			// and the resulting value is stored in the right half.
			rSide = prevLSide ^ feistelFunc.feistel(rSide, subkeys[ii]);
		}
		
		concatRL = switchFunc.concatToRL(lSide, rSide);
		
		// apply the final permutation
		finalPerm = perm.finalPermutation(concatRL);
		
		// return the cipherText
		return finalPerm;

	}
}