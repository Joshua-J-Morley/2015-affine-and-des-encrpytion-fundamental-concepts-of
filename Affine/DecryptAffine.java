import java.util.*;
import java.io.*;

public class DecryptAffine
{
	private int a;
	private int b;
	private static Scanner input = new Scanner(System.in);
	private AffineFunctions func = new AffineFunctions();

	public DecryptAffine()
	{
		a = 0;
		b = 0;
	}

	public DecryptAffine(int inA, int inB)
	{
		a = inA;
		b = inB;
	}

	public void decrypt()
	{
		String fileName, decryptedText, text = "";
		int keyA, keyB;

		System.out.println("Enter fileName: ");
		fileName = input.next();
		try
		{
			text = func.readFile(fileName);
		}
		catch(IOException e)
		{
			System.out.println("Could not read from file" + e.getMessage());
		}

		setA(func.getKeyA());
		setB(func.getKeyB());

		if(func.keyValid(a, b))
		{
			decryptedText = decryptMessage(text);
			System.out.println("Decrypted plaintext: " + decryptedText);
			try
			{
				System.out.println("Enter fileName to save as: ");
				fileName = input.next();
				func.writeFile(fileName, decryptedText);
			}
			catch(IOException e)
			{
				System.out.println("Could not write to file" + e.getMessage());
			}
		}
		else
		{
			System.out.println("Given key is not eligible");
		}
	}

	public String decryptMessage(String inCipherText)
	{
		int aInverse = 0;
		char cipherChar, plainChar = 'a';
		String outPlainText = new String("");

		for (int ii = 1; ii < 26; ii++) 
		{
			if(((ii * a) % 26) == 1)
			{
				aInverse = ii;
				//System.out.println("-----aInverse: " + aInverse);
			}
		}

		for (int kk = 0; kk < inCipherText.length(); kk ++) 
		{
			cipherChar = inCipherText.charAt(kk);
			if(Character.isLetter(cipherChar))
			{
				plainChar = (char)((aInverse * (cipherChar - 97 - b + 26) % 26)+97);
				outPlainText = outPlainText + plainChar;
			}
			
		}

		return outPlainText;
	}

	public void setA(int inA)
	{
		a = inA;
	}

	public void setB(int inB)
	{
		b = inB;
	}
}
