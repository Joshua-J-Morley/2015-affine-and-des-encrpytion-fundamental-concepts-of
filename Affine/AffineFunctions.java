import java.util.*;
import java.io.*;

public class AffineFunctions
{
	private static Scanner input = new Scanner(System.in);

	public String readFile(String inFileName) throws IOException
	{
		String line, outPlainText = "";
		BufferedReader reader;

		reader = new BufferedReader(new FileReader(inFileName));

		line = reader.readLine();
		while(line != null)
		{
			outPlainText = outPlainText + line + ":";
			line = reader.readLine();
		}
		reader.close();
		//System.out.println("text after read" + outPlainText);

		return outPlainText;
	}

	public void writeFile(String inFileName, String inText) throws IOException
	{
		PrintWriter writer = new PrintWriter(inFileName);
		writer.print(inText);
		writer.close();
	}

	public int getKeyA()
	{
		int outA;

		System.out.println("\nEnter Key A: ");
		outA = input.nextInt();

		if(outA < 0)
		{
			outA = 90;
		}

		return outA;
	}

	public int getKeyB()
	{
		int outB;

		System.out.println("Enter Key B: ");
		outB = input.nextInt();

		if(outB > 25)
		{
			outB = 90;
		}

		return outB;
	}

	public boolean keyValid(int inKeyA, int inKeyB)
	{
		boolean isValid, keyAValid, keyBValid, aCoPrime;

		isValid = false;
		keyAValid = (inKeyA == 90);
		keyBValid = (inKeyB == 90);
		aCoPrime = (greatestCommonFactor(26, inKeyA) == 1);

		if((keyAValid == false) && (keyBValid == false))
		{
			if (aCoPrime == true) 
			{
				isValid = true;	
			}
			else
			{
				System.out.println("A is not coprime");
			}
		}
		else if(keyAValid == true)
		{
			System.out.println("B is not valid");
		}
		else if(keyBValid == true)
		{
			System.out.println("A is not valid");
		}
		else
		{
			System.out.println("something went wrong");
		}

		return isValid;
	}

	public int greatestCommonFactor(int inA, int inB)
	{
		int retVal; 

		if(inB == 0)
		{
			retVal = inA;
		}
		else
		{
			retVal = greatestCommonFactor(inB, inA%inB);
		}

		return retVal;
	}

	public int[] getFrequency(char[] chars)
	{
		int[] frequency = new int[26];
		
		for(int ii = 0; ii < chars.length; ii ++)
		{
			if(Character.isLetter(chars[ii]))
			{
				char temp = Character.toLowerCase(chars[ii]);
				frequency[temp - 'a'] ++;
				//System.out.println(temp);
			}
		}
		printFrequency(frequency);
		return frequency;
	}

	public void printFrequency(int[] frequency)
	{
		char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();

		for(int ii = 0; ii < frequency.length; ii++)
		{
			System.out.print(alphabet[ii]);
			System.out.println(": occured :" + frequency[ii] + " times.");
		}
	}
}