import java.util.*;
import java.io.*;

public class EncryptAffine
{
	private int a;
	private int b;
	private static Scanner input = new Scanner(System.in);
	private AffineFunctions func = new AffineFunctions();

	public EncryptAffine()
	{
		a = 0;
		b = 0;
	}

	public EncryptAffine(int inA, int inB)
	{
		a = inA;
		b = inB;
	}

	public void encrypt()
	{
		String fileName, cipherText, text = "";
		int keyA, keyB;
		int[] letterFreq;

		System.out.println("\nEnter fileName: ");
		fileName = input.next();
		try
		{
			text = func.readFile(fileName);
		}
		catch(IOException e)
		{
			System.out.println("Could not read from file" + e.getMessage());
		}

		//letterFreq = func.getFrequency(text.toCharArray())

		setA(func.getKeyA());
		setB(func.getKeyB());

		if(func.keyValid(a, b))
		{
			//System.out.println("text before encrypt" + text);
			cipherText = encryptMessage(text);
			System.out.print("\nEncrypted CipherText: ");
			System.out.print(cipherText + "\n");
			try
			{
				System.out.println("Enter fileName to save as: ");
				fileName = input.next();
				func.writeFile(fileName, cipherText);
			}
			catch(IOException e)
			{
				System.out.println("Could not write to file" + e.getMessage());
			}

		}
		else
		{
			System.out.println("Given key is not eligible");
		}
	}

	public String encryptMessage(String inPlainText)
	{
		char plainChar, cipherChar = 'a';
		String outCipherText = new String("");
		//System.out.println("plain text in encrypt" + inPlainText);

		for (int ii = 0; ii < inPlainText.length(); ii ++) 
		{
			plainChar = inPlainText.charAt(ii);
			if(Character.isLetter(plainChar)) 
			{
				plainChar = Character.toLowerCase(plainChar);

				cipherChar = (char)(((a * (plainChar - 97) + b) % 26)+97);
				if (cipherChar > 'z')
				{
					cipherChar -= 26;	
				}
				else if(cipherChar < 'a')
				{
					cipherChar += 26;
				}
				outCipherText = outCipherText + cipherChar;
			}
			
		}

		return outCipherText;
	}

	public void setA(int inA)
	{
		a = inA;
	}

	public void setB(int inB)
	{
		b = inB;
	}
}
