import java.util.*;
import java.io.*;

public class Controller
{
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) 
	{
		int encOrDec, done = 1;
		EncryptAffine encrypt = new EncryptAffine();
		DecryptAffine decrypt = new DecryptAffine();

		//System.out.println("Enter plaintext: ");
		//text = input.next();
		while(done != 2)
		{
			System.out.println("\nWould you like to: ");
			System.out.println("Encrypt: 1");
			System.out.println("Decrypt: 2");
			encOrDec = input.nextInt();

			if(encOrDec == 1)
			{
				encrypt.encrypt();
			}
			else if(encOrDec == 2)
			{
				decrypt.decrypt();
			}
			else
			{
				System.out.println("Invalid Option");
			}

			System.out.println("\nWould you like do something else?: ");
			System.out.println("Yes: 1");
			System.out.println("No: 2");
			done = input.nextInt();
		}
	}
}